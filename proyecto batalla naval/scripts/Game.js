var Game = function() {
  this.board_size = 10;
  this.player = new Player(this.board_size);
  this.enemy;
  this.state = 'setup';
};
Game.prototype.create_setup_board = function(jq_container) {
  var board = new Board(this, this.board_size, true);
  jq_container.append(board.render().el);
};
Game.prototype.board_click = function(x, y) {
  if(this.state === 'setup'){
    this.add_ship(x, y);
  } else if(this.state === 'play'){
    if(this.enemy.board[y][x] === 1){
      jQuery(jQuery('td', jQuery(jQuery('#enemy-board').find('tr')[y]))[x]).addClass('hit');
    } else{
      jQuery(jQuery('td', jQuery(jQuery('#enemy-board').find('tr')[y]))[x]).addClass('miss');
    };
  }
};
Game.prototype.add_ship = function(x, y) {
  
};
Game.prototype.start_game = function() {
  jQuery('#game-setup').css({'display': 'none'});
  jQuery('#game-play').css({'display': 'block'});

  var player_board = new Board(this, this.board_size, false);
  jQuery('#player-board').append(player_board.render().el);
  player_board.populate(this.player.board);

  this.enemy = new Player();
  this.enemy.board = this.player.board.reverse();

  var enemy_board = new Board(this, this.board_size, true);
  jQuery('#enemy-board').append(enemy_board.render().el);
};