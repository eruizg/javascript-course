/**
 * Created by esteban on 7/13/2014.
 */
function setup(ans) {
    var lit = '';
    if (ans == 'anim') {
        lit = 'How many legs ? ';
        lit = lit + '<SELECT NAME="q2" ONCHANGE="alert(document.quest.q2.value)">';
        lit = lit + '<OPTION VALUE="">- Please select -</OPTION>';
        lit = lit + '<OPTION VALUE="cat">4</OPTION>';
        lit = lit + '<OPTION VALUE="sparrow">2</OPTION>';
        lit = lit + '<OPTION VALUE="snake">0</OPTION>';
        lit = lit + '</SELECT>';
    }
    if (ans == 'min') {
        lit = 'What colour ? ';
        lit = lit + '<SELECT NAME="q2" ONCHANGE="alert(document.quest.q2.value)">';
        lit = lit + '<OPTION VALUE="">- Please select -</OPTION>';
        lit = lit + '<OPTION VALUE="emerald">green</OPTION>';
        lit = lit + '<OPTION VALUE="ruby">red</OPTION>';
        lit = lit + '<OPTION VALUE="sapphire">blue</OPTION>';
        lit = lit + '</SELECT>';
    }
    document.getElementById('rep').innerHTML=lit;
}
